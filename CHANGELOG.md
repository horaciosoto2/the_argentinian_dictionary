# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-07-01
### Added
- Added CSV file with 185 phrases from The Argentinian Dictionary.
- Added custom random function.
- Added Instagram embedded post from [@theargentiniandictionary](https://www.instagram.com/theargentiniandictionary/).
- Added Docker integration.
