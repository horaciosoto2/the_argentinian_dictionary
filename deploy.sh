echo -e '\e[1m\e[34mStarting deployment\e[0m\n'

echo -e '\e[1m\e[34mMoving to path\e[0m'
cd ~/projects/utn/the-argentinian-dictionary
echo -e $PWD

echo -e '\n\e[1m\e[34mStopping container\e[0m'
docker stop $1

echo -e '\n\e[1m\e[34mDeleting container\e[0m'
docker rm $1

echo -e '\n\e[1m\e[34mRunning the new container\e[0m'
docker run --name $1 --restart always --env-file ./.env --env APP_VERSION=$3 -p 8081:8081 -d $2

echo -e '\n\e[1m\e[34mStarting application\e[0m'
docker exec -it -d $1 symfony server:start --port=8081 --no-tls
docker ps

echo -e '\n\e[1m\e[34mAll done!\e[0m'