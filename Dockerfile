FROM php:7.4-fpm-alpine

RUN apk update \
    && apk add  --no-cache \
    git \
    curl \
    bash \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && rm -rf /tmp/*

RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN composer install --no-dev
