<?php

namespace App\Tests\Util;

use App\Util\App as AppUtil;
use PHPUnit\Framework\TestCase;

final class AppTest extends TestCase
{
    /**
     * @var mixed extended class of App\Util\App
     */
    protected $appUtil;

    protected function setUp(): void
    {
        $this->appUtil = new class extends AppUtil {
            public function returnThis()
            {
                return $this;
            }
        };
    }

    public function testAbstractClass()
    {
        $this->assertInstanceOf(AppUtil::class, $this->appUtil->returnThis());
    }

    public function testDefaultDictionaryPath()
    {
        $this->assertTrue(is_string(AppUtil::DEFAULT_DICTIONARY_PATH));
        $this->assertEquals('.csv', substr(AppUtil::DEFAULT_DICTIONARY_PATH, -4));
    }

    public function testDefaultDictionarySize()
    {
        $this->assertTrue(is_int(AppUtil::DEFAULT_DICTIONARY_SIZE));
        $this->assertGreaterThan(1, AppUtil::DEFAULT_DICTIONARY_SIZE);
        $this->assertEquals(185, AppUtil::DEFAULT_DICTIONARY_SIZE);
    }

    public function testGetPhraseList()
    {
        $list = AppUtil::getPhraseList(AppUtil::DEFAULT_DICTIONARY_PATH);

        $this->assertTrue(is_array($list));
        $this->assertEquals(AppUtil::DEFAULT_DICTIONARY_SIZE, count($list));
    }

    public function testGetPhrase()
    {
        $phrase = AppUtil::getPhrase(10);

        $this->assertTrue(is_array($phrase));
        $this->assertEquals(3, count($phrase));
        $this->assertEquals('al toque, perro', $phrase['spanish']);
        $this->assertEquals('to the touch, dog!', $phrase['english']);
        $this->assertEquals('https://www.instagram.com/p/CDMHs53gpb3/', $phrase['link']);
    }

    public function testGetRandomPhrase()
    {
        $random = AppUtil::getRandomPhrase();

        $this->assertTrue(is_array($random));
        $this->assertEquals(3, count($random));
        $this->assertTrue(is_string($random['spanish']));
        $this->assertTrue(is_string($random['english']));
        $this->assertTrue(is_string($random['link']));
    }
}
