<?php

namespace App\Util;

abstract class App
{
    /**
     * CSV file path
     */
    const DEFAULT_DICTIONARY_PATH = '../Resources/dictionary.csv';

    /**
     * Max number of positions in phrases list
     */
    const DEFAULT_DICTIONARY_SIZE = 185;

    /**
     * Get the list of the argentinian phrases from a file
     *
     * @param string $path
     * @return array
     */
    public static function getPhraseList(string $path): array
    {
        $data  = file_get_contents(dirname(__FILE__) . DIRECTORY_SEPARATOR . $path);
        $lines = explode(PHP_EOL, $data);
        $list  = [];

        foreach ($lines as $line) {
            $list[] = array_combine(['spanish', 'english', 'link'], str_getcsv($line));
        }

        return $list;
    }

    /**
     * Get a phrase by its position
     *
     * @param integer $position
     * @return array
     */
    public static function getPhrase(int $position): array
    {
        $list = self::getPhraseList(self::DEFAULT_DICTIONARY_PATH);

        return $list[$position];
    }

    /**
     * Get a random phrase
     *
     * @return array
     */
    public static function getRandomPhrase(): array
    {
        return self::getPhrase(random_int(0, self::DEFAULT_DICTIONARY_SIZE));
    }
}
