<?php

namespace App\Controller;

use App\Util\App as AppUtil;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(): Response
    {
        return $this->render('app/index.html.twig', [
            'phrase'      => AppUtil::getRandomPhrase(),
            'app_version' => getenv('APP_VERSION') ?? '',
        ]);
    }
}
